# stringsbuilder tool: Short description & copyright/warranty notice #

## INFO ##

This project is not maintained and may include outdated information or code that is
not compatible with recent versions of tools, such as, Xcode and CocoaPods. The
repository is online for reference only.

For current projects, please visit my [GitHub](https://github.com/shagedorn) or
[Stackoverflow](https://stackoverflow.com/users/2050985/hagi) profiles.

stringsbuilder allows you to keep and administrate your **localised strings resources**
in Microsoft Excel-compatible CSV files while generating a target-specific .strings
file during the build process. It supports CSV files that are distributed across
multiple directories and shared/target-specific CSV files. Source and target string
encoding are configurable, a sample project directory structure is included. For
documentation, please see the example run script or the source files.

stringsbuilder has been developed as part of this project, in which it is used, too:  
https://bitbucket.org/shagedorn/modular-erp-app

**Runs on:** Mac OS X 10.7
**Useful for:** Xcode projects

## LEGAL ##

### COPYRIGHT & WARRANTY NOTICE (stringsbuilder) ###

stringsbuilder was written by Sebastian Hagedorn as part of a study work at
TU Dresden/SALT Solutions GmbH. It was published under the following license (FreeBSD):

> Copyright (c) 2012 Sebastian Hagedorn
> All rights reserved.
> 
> Redistribution and use in source and binary forms, with or without modification,
> are permitted provided that the following conditions are met:
> 
> 1. Redistributions of source code must retain the above copyright notice, this
> list of conditions and the following disclaimer.
> 2. Redistributions in binary form must reproduce the above copyright notice, this
> list of conditions and the following disclaimer in the documentation and/or other
> materials provided with the distribution.
> 
> THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
> ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
> WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
> IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
> INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
> BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
> DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
> LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
> OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
> THE POSSIBILITY OF SUCH DAMAGE.

### COPYRIGHT & WARRANTY NOTICE (CHCSVParser) ###

This software (stringsbuilder) includes the CHCSVParser library which was published
under the following license (MIT):

> Copyright (c) 2011 Dave DeLong
> 
> Permission is hereby granted, free of charge, to any person obtaining a copy of
> this software and associated documentation files (the "Software"), to deal in the
> Software without restriction, including without limitation the rights to use, copy,
> modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
> and to permit persons to whom the Software is furnished to do so, subject to the
> following conditions:
> 
> The above copyright notice and this permission notice shall be included in all
> copies or substantial portions of the Software.
> 
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
> INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
> PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
> HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
> OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
> SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

https://github.com/davedelong/CHCSVParser}