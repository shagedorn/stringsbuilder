//
//  StaticHelperClass.h
//  stringsbuilder
//
//  Created by Sebastian Hagedorn on 08.10.12.
//
//

#import <Foundation/Foundation.h>

@interface StaticHelperClass : NSObject

// extract csv file names from a parsed plist file
+ (void) getCSVFileNameFromPLIST:(NSDictionary*)plist
                          srcDir:(NSString*)srcDirPath
                           addTo:(NSMutableArray*)csvFileNames;

@end
