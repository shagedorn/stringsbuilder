//
//  main.m
//  StringsBuilder
//
//  Created by Sebastian Hagedorn on 18.10.12.
//
//

// For more documentation, see TestRunT1.sh in /SampleDir

#import <Foundation/Foundation.h>
#import "CHCSV.h"
#import "StringFileEntry.h"
#import "StaticHelperClass.h"

#pragma mark - Definitions

#define CSV_INDEX_HEAD 0
#define CSV_INDEX_KEY 0
#define CSV_INDEX_COMMENT 1

#pragma mark - Default values

#define CSV_DEFAULT_DELIMITER @","
#define DEFAULT_TARGET_FILE_NAME @"SBGeneratedStrings"
#define DEFAULT_SOURCE_ENCODING NSMacOSRomanStringEncoding
#define DEFAULT_TARGET_ENCODING NSUTF8StringEncoding

#pragma mark - Script-ish Code

int main(int argc, const char * argv[])
{

    @autoreleasepool {

#pragma mark - Parameters

        // [1] Root directory of the project you want to localise (absolute path)
        // [2] Relative path to common PLIST file (use '0' to skip)
        // [3] Relative path to tenant-specific PLIST file (use '0' to skip)
        // [4] Relative path to directory where the localised .strings file lives

        // all paths but [1] are relativ paths to [1]

        NSLog(@"argc: %d", argc);
        if (argc < 5) {
            NSLog(@"Mandatory parameters missing. See source code for documentation.");
            return 1;
        }

        NSString *sourceDirPath = [NSString  stringWithUTF8String:argv[1]];
        NSString *commonPlistPath = [[NSString stringWithFormat:@"%@", sourceDirPath] stringByAppendingPathComponent:[NSString stringWithUTF8String:argv[2]]];
        NSString *tenantPlistPath = [[NSString stringWithFormat:@"%@", sourceDirPath] stringByAppendingPathComponent:[NSString stringWithUTF8String:argv[3]]];
        NSString *resourcesPath = [[NSString stringWithFormat:@"%@", sourceDirPath] stringByAppendingPathComponent:[NSString stringWithUTF8String:argv[4]]];

        NSLog(@"Mandatory parameters set.");
        NSLog(@"Project Root Directory: %@", sourceDirPath);
        NSLog(@"Common PLIST: %@", commonPlistPath);
        NSLog(@"Target PLIST: %@", tenantPlistPath);
        NSLog(@"Resources: %@", resourcesPath);

        // Optional parameters:

        // [5] CSV source file encoding (default: 30 (NSMacOSRomanStringEncoding))
        // [6] .strings target file encoding (default: 4 (NSUTF8StringEncoding))
        // [7] Target file name (default: SBGeneratedStrings – file extension is added by the tool)
        // [8] CSV delimiter (default: ",")

        // File encodings must be specified as integers according to the NSStringEncoding enum:
        // https://developer.apple.com/library/mac/documentation/Cocoa/Reference/Foundation/Classes/NSString_Class/Reference/NSString.html#//apple_ref/doc/constant_group/String_Encodings

        NSStringEncoding usedEncoding = DEFAULT_SOURCE_ENCODING;
        if (argc > 5) {
            NSString *encodingAsString = [NSString stringWithUTF8String:argv[5]];
            usedEncoding = [encodingAsString intValue];
            NSLog(@"Source Encoding set: %ld", usedEncoding);
        }

        NSStringEncoding targetEncoding = DEFAULT_TARGET_ENCODING;
        if (argc > 6) {
            NSString *encodingAsString = [NSString stringWithUTF8String:argv[6]];
            targetEncoding = [encodingAsString intValue];
            NSLog(@"Target Encoding set: %ld", targetEncoding);
        }

        NSString *fileName = DEFAULT_TARGET_FILE_NAME;
        if (argc > 7) {
            fileName = [NSString stringWithUTF8String:argv[7]];
            NSLog(@"Target file name set: %@", fileName);
        }

        NSString *csvDelimiter = CSV_DEFAULT_DELIMITER;
        if (argc > 8) {
            csvDelimiter = [NSString stringWithUTF8String:argv[8]];
            NSLog(@"Delimiter set: %@", csvDelimiter);
        }

#pragma mark - Read PLISTs

        NSDictionary *commonPlist = [NSDictionary dictionaryWithContentsOfFile:commonPlistPath];
        if (!commonPlist) {
            if ([commonPlistPath intValue] == 0) {
                // Parameter skipped
                NSLog(@"No common PLIST used.");
            } else {
                NSLog(@"Common PLIST could not be found or read: %@", commonPlistPath);
                return 1;
            }
        }

        NSDictionary *tenantPlist = [NSDictionary dictionaryWithContentsOfFile:tenantPlistPath];
        if (!tenantPlist) {
            if ([tenantPlistPath intValue] == 0) {
                // Parameter skipped
                NSLog(@"No tenant-specific PLIST used.");
            } else {
                NSLog(@"Tenant PLIST could not be found or read: %@", tenantPlistPath);
                return 1;
            }
        }
        
        NSMutableArray *csvFileNames = [NSMutableArray array];

        if (commonPlist) {
            [StaticHelperClass getCSVFileNameFromPLIST:commonPlist
                                                srcDir:sourceDirPath
                                                 addTo:csvFileNames];
        }
        if (tenantPlist) {
            [StaticHelperClass getCSVFileNameFromPLIST:tenantPlist
                                                srcDir:sourceDirPath
                                                 addTo:csvFileNames];
        }
       
#pragma mark - Save content temporarily

        // 1 base dictionary: 1 sub dictionary per language
        NSMutableDictionary *stringsFileContent = [NSMutableDictionary dictionary];

        // Read CSV files
        for (NSString* csvFileName in csvFileNames) {
            NSError *error = nil;

            NSArray *csvContent = [[[NSArray alloc] initWithContentsOfCSVFile:csvFileName
                                          usedEncoding:&usedEncoding
                                             delimiter:csvDelimiter
                                                 error:&error] autorelease];
            
            if (error || !csvContent) {
                NSLog(@"Error: %@ in file: %@", error, csvFileName);
                return 1;
            }

            CFStringRef str = CFStringGetNameOfEncoding(CFStringConvertNSStringEncodingToEncoding(usedEncoding));
            NSLog(@"CSV Source Encoding (%@): %ld (%@)", [csvFileName lastPathComponent], usedEncoding, (NSString*)str);

            // pro record 1 sub array, headline in first line
            // next: collect keys
            // Assumption: tenant-specific files are read after the base file
            // ==> tenant-specific values overwrite base values for common keys

            // remember column/language mapping per file
            NSMutableArray* languageIndexMapping = [NSMutableArray arrayWithCapacity:4];

            [csvContent enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                NSArray *row = (NSArray*)obj;

                if (idx == CSV_INDEX_HEAD) {
                    // analyse headline for available languages
                    [row enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                        // ignore first two rows
                        if (idx > CSV_INDEX_COMMENT) {

                            NSString *language = (NSString*)obj;
                            if (![stringsFileContent objectForKey:language]) {
                                // new language
                                NSMutableDictionary *languageInfos = [NSMutableDictionary dictionary];
                                [stringsFileContent setObject:languageInfos forKey:language];
                            }
                            // add to temp. mapping
                            [languageIndexMapping addObject:language];
                        }
                    }];

                } else {
                    // save content of 1 row
                    NSString *key = nil;
                    NSString *comment = nil;

                    for (int i = 0; i < row.count; i++) {
                        if (i == CSV_INDEX_KEY) {
                            key = row[i];
                        } else if (i == CSV_INDEX_COMMENT) {
                            comment = row[i];
                        } else {
                            int languageIndex = i - 2; // ignore first two columns
                            NSString *languageKey = languageIndexMapping[languageIndex];

                            StringFileEntry *entry = [[StringFileEntry alloc] init];
                            entry.key = key;
                            entry.comment = comment;
                            entry.language = languageKey;
                            entry.localizedContent = row[i];

                            NSMutableDictionary *languageInfos = [stringsFileContent objectForKey:languageKey];
                            [languageInfos setObject:entry forKey:key];

                            [entry release];
                        }
                    }

                }
            }];
        }

#pragma mark - Create output

        for (NSString *languageKey in stringsFileContent.allKeys) {
            NSString *path = [resourcesPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.lproj", languageKey]];
            path = [path stringByAppendingPathComponent:fileName];
            path = [path stringByAppendingPathExtension:@"strings"];

            NSMutableString* fileContent = [NSMutableString string];
            NSDictionary *languageSpecificContent = [stringsFileContent valueForKey:languageKey];
            for (NSString* key in languageSpecificContent.allKeys) {
                StringFileEntry *entry = [languageSpecificContent valueForKey:key];
                [fileContent appendFormat:@"%@\n", [entry toStringsFileTextEntry]];
            }

            NSLog(@"Write %@ to %@", fileContent, path);

            NSError *writeError = nil;
            [fileContent writeToFile:path atomically:YES encoding:targetEncoding error:&writeError];
            if (writeError) {
                if (writeError.domain == NSCocoaErrorDomain && writeError.code == 4) {
                    // .lproj folders do not exist
                    NSString *dirPath = [path stringByDeletingLastPathComponent];
                    BOOL success = [[NSFileManager defaultManager] createDirectoryAtPath:dirPath
                                              withIntermediateDirectories:NO
                                                               attributes:nil
                                                                    error:&writeError];
                    if (success) {
                        [fileContent writeToFile:path atomically:YES encoding:targetEncoding error:&writeError];
                        NSLog(@"Created directory: %@", dirPath);
                    } else {
                        NSLog(@"Failed to create directory: %@", writeError);
                        return 1;
                    }

                } else {
                    NSLog(@"Write Error: %@", writeError);
                    return 1;
                }
            }
        }
        
    }
    NSLog(@"Done.");
    return 0;
}
