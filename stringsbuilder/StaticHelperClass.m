//
//  StaticHelperClass.m
//  stringsbuilder
//
//  Created by Sebastian Hagedorn on 08.10.12.
//
//

#import "StaticHelperClass.h"

@implementation StaticHelperClass

// extract csv file names from a parsed plist file
+ (void) getCSVFileNameFromPLIST:(NSDictionary*)plist
                          srcDir:(NSString*)srcDirPath
                           addTo:(NSMutableArray*)csvFileNames {

    for (NSString *key in plist.allKeys) {
        id value = [plist valueForKey:key];
        if ([value isKindOfClass:[NSArray class]]) {
            for (NSString *fileName in value) {
                NSLog(@"Use CSV file: %@/%@", srcDirPath, fileName);
                [csvFileNames addObject:[NSString stringWithFormat:@"%@/%@", srcDirPath, fileName]];
            }
        }
    }

}

@end
