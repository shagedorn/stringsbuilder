#!/bin/sh

#  TestRun.sh
#  stringsbuilder
#
#  Created by Sebastian Hagedorn on 18/10/12.

# Test Run for sample tenant 1
# Note: You need to build the tool (target: stringsbuilder) first!

# Referenced in Run Script Build Phase of the target (here: TestRunT1 target)
# Can be embedded (parameter adaption necessary) into tenant-specific target of any iOS project
# The Run script phase must be executed before the Copy Bundle Resources phase starts
# This allows for adding the yet-to-be-updated SBGeneratedStrings.strings file to the copy resources phase of all targets
# The content will be updated for a specific target during the Run Script phase

# Neither the CSV nor the PLIST files need to be part of any target, just one .strings file!

# Tool is installed to $CACHE_ROOT/stringsbuilder by default
# See Console.app for output

$CACHE_ROOT/stringsbuilder/stringsbuilder $SOURCE_ROOT/stringsbuilder/SampleDir Resources/BaseStrings.plist Tenant1Res/StringFiles.plist Resources 30 4 SBGeneratedStrings ","

# Parameters: (Order is important!)

# [1] Root directory of the project you want to localise (absolute path)
# [2] Relative path to common PLIST file where CSVs are referenced
# [3] Relative path to tenant-specific PLIST file
# [4] Relative path to directory where the localised .strings file lives **

# Use '0' as parameter value to skip [2] or [3]
# See TestRunT2 target/Run Script phase for an example

# Optional parameters:

# [5] CSV source file encoding (default: 30 (NSMacOSRomanStringEncoding))
# [6] .strings target file encoding (default: 4 (NSUTF8StringEncoding))
# [7] Target file name (default: SBGeneratedStrings – file extension is added by the tool)
# [8] CSV delimiter (default: ",") - make sure to wrap semicolons in quotation marks

# all paths but [1] are relativ paths to [1]

# File encodings must be specified as integers according to the NSStringEncoding enum:
# https://developer.apple.com/library/mac/documentation/Cocoa/Reference/Foundation/Classes/NSString_Class/Reference/NSString.html#//apple_ref/doc/constant_group/String_Encodings