//
//  StringFileEntry.h
//  MRS
//
//  Created by Sebastian Hagedorn on 11.10.12.
//
//

#import <Foundation/Foundation.h>

@interface StringFileEntry : NSObject

@property (retain) NSString* language;
@property (retain) NSString* comment;
@property (retain) NSString* key;
@property (retain) NSString* localizedContent;

- (NSString*) toStringsFileTextEntry;

@end
