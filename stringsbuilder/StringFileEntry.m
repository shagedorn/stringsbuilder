//
//  StringFileEntry.m
//  MRS
//
//  Created by Sebastian Hagedorn on 11.10.12.
//
//

#import "StringFileEntry.h"

@implementation StringFileEntry

- (NSString *)toStringsFileTextEntry {
    NSString *firstLine = @"";
    if (self.comment && ![self.comment isEqualToString:@""]) {
        firstLine = [NSString stringWithFormat:@"/* %@ */\n", self.comment];
    }
    NSString *secondLine = [NSString stringWithFormat:@"\"%@\" = \"%@\";", self.key, self.localizedContent];
    return [NSString stringWithFormat:@"%@%@", firstLine, secondLine];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"Lg %@ cm %@ key %@ ct %@", self.language, self.comment, self.key, self.localizedContent];
}

-(void)dealloc {
    self.language = nil;
    self.comment = nil;
    self.key = nil;
    self.localizedContent = nil;
    [super dealloc];
}

@end
